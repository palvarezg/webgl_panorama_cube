import * as THREE from './build/three.module.js';

export default class ImageFactory {
    window;
    divName = '';
    textureUri = '';
    camera;
    scene;
    renderer;
    isUserInteracting = false;
    onPointerDownMouseX = 0;
    onPointerDownMouseY = 0;
    onPointerDownLon = 0;
    onPointerDownLat = 0;
    lon = 0;
    lat = 0;
    phi = 0;
    theta = 0;

    constructor(divName, textureUri) {
        this.divName = divName;
        this.textureUri = textureUri;

        if (this.divName != '' && this.textureUri != '') {
            this.init();
            this.animate();
        }
    }

    init() {
        const container = document.getElementById(this.divName);
        if (container === null) return;

        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1100);
        this.scene = new THREE.Scene();

        // invert the geometry on the x-axis so that all of the faces point inward
        const geometry = new THREE.SphereBufferGeometry(500, 60, 40);
        geometry.scale(- 1, 1, 1);

        const texture = new THREE.TextureLoader().load(this.textureUri);
        const material = new THREE.MeshBasicMaterial({ map: texture });
        const mesh = new THREE.Mesh(geometry, material);
        this.scene.add(mesh);

        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(window.innerWidth / 2, window.innerHeight / 2);

        container.appendChild(this.renderer.domElement);
        container.style.touchAction = 'none';
        container.addEventListener('pointerdown', this.onPointerDown, false);

        document.addEventListener('wheel', this.onDocumentMouseWheel, false);
        document.addEventListener('dragover', (event) => {
            event.preventDefault();
            event.dataTransfer.dropEffect = 'copy';
        }, false);

        document.addEventListener('dragenter', () => {
            document.body.style.opacity = 0.5;
        }, false);

        document.addEventListener('dragleave', () => {
            document.body.style.opacity = 1;
        }, false);

        document.addEventListener('drop', (event) => {
            event.preventDefault();
            const reader = new FileReader();
            reader.addEventListener('load', (event) => {
                material.map.image.src = event.target.result;
                material.map.needsUpdate = true;
            }, false);
            reader.readAsDataURL(event.dataTransfer.files[0]);
            document.body.style.opacity = 1;
        }, false);

        window.addEventListener('resize', this.onWindowResize, false);
    }

    onWindowResize() {
        if (this.camera === undefined) return;
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(window.innerWidth / 2, window.innerHeight / 2);
    }

    onPointerDown(event) {
        if (event.isPrimary === false) return;
        this.isUserInteracting = true;
        this.onPointerDownMouseX = event.clientX;
        this.onPointerDownMouseY = event.clientY;
        this.onPointerDownLon = this.lon;
        this.onPointerDownLat = this.lat;
        document.addEventListener('pointermove', this.onPointerMove, false);
        document.addEventListener('pointerup', this.onPointerUp, false);
    }

    onPointerMove(event) {
        if (event.isPrimary === false) return;
        this.lon = (this.onPointerDownMouseX - event.clientX) * 0.1 + this.onPointerDownLon;
        this.lat = (event.clientY - this.onPointerDownMouseY) * 0.1 + this.onPointerDownLat;
    }

    onPointerUp() {
        if (event.isPrimary === false) return;
        this.isUserInteracting = false;
        document.removeEventListener('pointermove', this.onPointerMove);
        document.removeEventListener('pointerup', this.onPointerUp);
    }

    onDocumentMouseWheel(event) {
        if (this.camera !== undefined) {
            const fov = this.camera.fov + event.deltaY * 0.05;
            this.camera.fov = THREE.MathUtils.clamp(fov, 10, 75);
            this.camera.updateProjectionMatrix();
        }
    }

    animate() {
        requestAnimationFrame(() => this.animate());
        this.update();
    }

    update() {
        if (this.isUserInteracting === false) {
            this.lon += 0.1;
        }

        this.lat = Math.max(- 85, Math.min(85, this.lat));
        this.phi = THREE.MathUtils.degToRad(90 - this.lat);
        this.theta = THREE.MathUtils.degToRad(this.lon);

        const x = 500 * Math.sin(this.phi) * Math.cos(this.theta);
        const y = 500 * Math.cos(this.phi);
        const z = 500 * Math.sin(this.phi) * Math.sin(this.theta);

        this.camera.lookAt(x, y, z);
        this.renderer.render(this.scene, this.camera);
    }
}